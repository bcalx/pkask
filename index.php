<?php

/**
 * Start application with initialize autoloader and routing
 *
 * Creating router instance process
 * gets possible routes from application/routes/routes.php file
 */

use application\core\Router;

spl_autoload_register(function ($class) {
	$path = str_replace('\\', '/', $class.'.php');
	if(file_exists($path))
		require_once $path;
});

$router = new Router;
$router->run();