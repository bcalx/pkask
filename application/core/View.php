<?php

/**
 * View class
 * Contains render html method
 */

namespace application\core;

class View
{
    public $route;
    public $path;
    private static $appViewPath = 'application/views';

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'] . '/' . $route['action'];
    }

    public function render($vars = [])
    {
        $pathAppFiles = self::$appViewPath . '/' . $this->path . '.php';

        extract($vars);

        if (file_exists($pathAppFiles)) {
            require $pathAppFiles;
        } else {
            echo "View not found " . $this->path;
        }
        exit;
    }
}