<?php

namespace application\core;

class Router
{
    protected $routes = [];
    protected $params = [];
    protected $dbConfig = [];

    function __construct()
    {
        $arrRoutes = require 'application/routes/routes.php';
        foreach ($arrRoutes as $key => $val) {
            $this->add($key, $val);
        }
    }

    /**
     * Prepare and add to routes property
     * validate for regexp routes
     *
     * @param $route
     * @param $params
     */
    public function add($route, $params)
    {
        $route = preg_replace('/{([a-z_]+):([^\}]+)}/', '(?P<\1>\2)', $route);
        $route = '#^'.$route.'$#';
        $this->routes[$route] = $params;
    }

    /**
     * Searching for matchers in request URL
     * if match, parse url by regular expression for parameters and set params property
     * if no return false
     *
     * @return bool
     */
    public function match()
    {
        $url = trim(explode('?', $_SERVER['REQUEST_URI'])[0], '/');
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        if (is_numeric($match)) {
                            $match = (int) $match;
                        }
                        $params[$key] = $match;
                    }
                }
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    /**
     * Run main application
     * if success matching url-pattern and parameters getting
     *  - get controller
     *  - creating controller instance also initializes model instance creating
     *    inside __construct
     *
     * if failed - set 404 error
     */
    public function run()
    {
        if ($this->match()) {
            $path = 'application\controllers\\'.ucfirst($this->params['controller']).'Controller';
            if (class_exists($path)) {
                $action = $this->params['action'].'Action';
                if(method_exists($path, $action)) {
                    $controller = new $path($this->params);
                    $controller->$action();
                }
            }
        }
        exit(http_response_code(404));
    }

}