<?php

/**
 * Main routes
 *
 * ['route_regexp({param_name: param_mask})' => ['controller_name', 'action_name']]
 */

return [
    '' => [
        'controller' => 'main',
        'action' => 'books'
    ],
    'get-book/{href:[a-z0-9\-\/]+}' => [
        'controller' => 'main',
        'action' => 'book'
    ],
];