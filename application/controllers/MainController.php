<?php

/**
 * Main controller for mini parsing book24
 * Render view with vars after parsing and exit application
 */

namespace application\controllers;

use simple_html_dom;

class MainController extends Controller
{
    public $domain = 'https://book24.ru';

    /**
     * Books list action
     */
	public function booksAction()
	{
	    $mainBooksPage = file_get_contents($this->domain . '/catalog/programmirovanie-1361/');
	    $booksDom = [];
        $booksDom[] = $this->getDom($mainBooksPage);
	    foreach ($booksDom[0]->find('a.catalog-pagination__item') as $page) {
            if (strcmp($page->innertext, 'Далее')) {
                $booksDom[] =  $this->getDom(file_get_contents($this->domain . $page->href));
            };
        }
        $booksList = [];
        foreach ($booksDom as $bookDom) {
            foreach ($bookDom->find('.book__content .book__title .book__title-link') as $book) {
                $booksList[] = [
                    'href' => trim($book->href),
                    'title' => trim($book->innertext),
                ];
            }
        }
        foreach ($booksDom as $bookDom) {
            $bookDom->clear();
        }
        unset($booksDom);
	    $this->view->render(['booksList' => $booksList]);
	}

    /**
     * Book single action
     */
    public function bookAction()
    {
        $book = [];
        $bookPage = $this->getDom(file_get_contents($this->domain . '/' . $this->route['href']));

        $book['img'] = trim($bookPage->find('.item-cover__image', 0)->attr['src']);

        $book['price'] = trim($bookPage->find('.item-actions__prices', 0)->find('.item-actions__price', 0)->first_child()->innertext);
        $book['title'] = trim($bookPage->find('.item-detail__title', 0)->innertext);

        foreach ($bookPage->find('.item-tab__chars-key') as $prop) {
            switch ($prop->innertext) {
                case 'Автор:' : {
                    $book['author'] = $this->getField($prop);
                    break;
                }
                case 'Год издания:' : {
                    $book['year'] = $this->getField($prop);
                    break;
                }
            }
        }

        $this->view->render(['book' => $book]);
    }

    /**
     * Additional method for getting dom object from str
     *
     * @param $html
     * @return simple_html_dom
     */
    public function getDom($html)
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . "/application/lib/simple_html_dom.php";
        $dom = new simple_html_dom();
        $dom->load($html);
        return $dom;
    }

    /**
     * Get field for author and year
     *
     * @param $prop
     * @return string
     */
    public function getField($prop)
    {
        if ($prop->next_sibling()->first_child()) {
            return trim($prop->next_sibling()->first_child()->innertext);
        } else {
            return trim($prop->next_sibling()->innertext);
        }
    }
}