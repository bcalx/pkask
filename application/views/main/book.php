<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        td {
            padding: 3rem;
        }
        td:nth-child(1) {
            font-weight: 600;
        }
    </style>
</head>
<body>
    <table border="1">
        <tbody>
        <tr>
            <td>Автор(ы)</td>
            <td><?=($book['author'] ?? '-')?></td>
        </tr>
        <tr>
            <td>Название</td>
            <td><?=($book['title'] ?? '-')?></td>
        </tr>
        <tr>
            <td>Цена в руб.</td>
            <td><?=($book['price'] ?? '-')?></td>
        </tr>
        <tr>
            <td>Год издания</td>
            <td><?=($book['year'] ?? '-')?></td>
        </tr>
        <tr>
            <td>Изображение</td>
            <td><img src="<?=$book['img']?>" alt=""></td>
        </tr>
        </tbody>
    </table>
</body>
</html>
